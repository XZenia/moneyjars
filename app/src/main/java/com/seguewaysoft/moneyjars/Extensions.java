package com.seguewaysoft.moneyjars;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.controller.JarController;
import com.seguewaysoft.moneyjars.model.Jar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Extensions {

    public static void showSnackbar(View view, String message, int length)
    {
        Snackbar.make(view, message, length)
                .setAction("Action", null).show();
    }

    public static void showToast(Context context, String message, int toastLength)
    {
        Toast.makeText(context, message, toastLength)
                .show();
    }

    public static double getRemainingAllocatedMoney(Context context)
    {
        EntryController entryController = new EntryController(context);
        JarController jarController = new JarController(context);
        List<Jar> jars = jarController.getAllJars();

        double remainingAllocatedMoney = 0;

        for(Jar jar: jars)
        {
            remainingAllocatedMoney += entryController.getTotalJarContent(jar.getId());
        }

        return remainingAllocatedMoney;
    }

    public static Calendar convertStringToTime(Context context, String time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Calendar tempTime = Calendar.getInstance();
        tempTime.setTimeInMillis(System.currentTimeMillis());
        try
        {
            SimpleDateFormat routineTimeFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
            tempTime.setTime(routineTimeFormat.parse(time));
            calendar.set(Calendar.HOUR_OF_DAY, tempTime.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, tempTime.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            return calendar;
        }
        catch (Exception exception)
        {
            Extensions.showToast(context, exception.getMessage(), Toast.LENGTH_SHORT);
            return calendar;
        }
    }

    public static String convertTimeToString(int hourOfDay, int minute, boolean twelveHourFormat)
    {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;

        if (twelveHourFormat)
        {
            String period = "AM";
            if (hourOfDay > 12)
            {
                hourOfDay -= 12;
                period = "PM";
            }
            else if (hourOfDay == 0)
            {
                hourOfDay += 12;
                period = "AM";
            }
            else if (hourOfDay == 12)
            {
                period = "PM";
            }

            hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;

            return hourString + ":" + minuteString + " " + period;
        }
        else
        {
           return hourString + ":" + minuteString;
        }
    }
}

package com.seguewaysoft.moneyjars.view.routine;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.RoutineExtensions;
import com.seguewaysoft.moneyjars.controller.JarController;
import com.seguewaysoft.moneyjars.controller.RoutineController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.model.Routine;
import com.seguewaysoft.moneyjars.adapter.CardViewListAdapter;
import com.seguewaysoft.moneyjars.adapter.RoutineRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class RoutineViewer extends AppCompatActivity {

    private RoutineController routineController;
    private TextView noEntriesTextView;

    private int jarId;

    private RecyclerView cardViewRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine_viewer);

        routineController = new RoutineController(RoutineViewer.this);
        jarId = getIntent().getExtras().getInt("jarId");

        noEntriesTextView = findViewById(R.id.NoEntriesTextView);

        FloatingActionButton addRoutineButton = findViewById(R.id.AddRoutineEntryButton);
        addRoutineButton.setOnClickListener(v -> {
            Intent addRoutine = new Intent(RoutineViewer.this, AddRoutine.class);
            addRoutine.putExtra("jarId", jarId);
            startActivityForResult(addRoutine, Constants.STATUS_OK);
        });

        JarController jarController = new JarController(RoutineViewer.this);

        getSupportActionBar().setTitle(jarController.getJar(jarId).getName() + " Routines");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cardViewRecyclerView = findViewById(R.id.CardViewRecyclerView);

        setupContentList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupContentList();
    }

    private void setupContentList()
    {
        List<Routine> routines = routineController.getJarRoutines(jarId);

        if (!routines.isEmpty())
        {
            List<LinearLayout> linearLayouts = new ArrayList<>();
            for (Routine routine: routines)
            {
                linearLayouts.add(createRecyclerViewElement(routine.getEntries(), routine));
            }
            cardViewRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            cardViewRecyclerView.setAdapter(new CardViewListAdapter(linearLayouts));
        }
        else
        {
            cardViewRecyclerView.setVisibility(View.GONE);
            noEntriesTextView.setVisibility(View.VISIBLE);
        }
    }

    private LinearLayout createRecyclerViewElement(List<Entry> list, Routine routine)
    {
        LinearLayout mainLinearLayout = new LinearLayout(RoutineViewer.this);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        mainLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mainLinearLayout.setLayoutParams(linearLayoutParams);

        LinearLayout headerLinearLayout = new LinearLayout(RoutineViewer.this);
        LinearLayout.LayoutParams headerLinearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        headerLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        headerLinearLayout.setLayoutParams(headerLinearLayoutParams);

        TextView routineListLabel = new TextView(RoutineViewer.this);
        LinearLayout.LayoutParams routineListLabelLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                50
        );
        routineListLabelLayoutParams.gravity = Gravity.CENTER_VERTICAL;
        routineListLabel.setLayoutParams(routineListLabelLayoutParams);
        routineListLabel.setTextColor(getResources().getColor(R.color.colorText, getTheme()));
        routineListLabel.setPadding(10,10,10,10);
        routineListLabel.setText(routine.getName());

        headerLinearLayout.addView(routineListLabel);

        Switch routineActiveSwitch = new Switch(RoutineViewer.this);
        routineActiveSwitch.setChecked(routine.isActive());

        LinearLayout.LayoutParams routineActiveSwitchLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        routineActiveSwitch.setLayoutParams(routineActiveSwitchLayoutParams);
        routineActiveSwitch.setChecked(routine.isActive());
        routineActiveSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            routineController.changeRoutineStatus(routine.getId(), isChecked);
            if (routineActiveSwitch.isChecked())
            {
                Routine updatedRoutine = RoutineExtensions.scheduleRoutine(RoutineViewer.this, routine);
                updatedRoutine.isActive(true);
                routineController.updateRoutine(updatedRoutine);
                Extensions.showToast(RoutineViewer.this,  getString(R.string.routine_enabled_message, routine.getName()), Toast.LENGTH_SHORT);
            }
            else
            {
                Routine updatedRoutine = RoutineExtensions.removeRoutineSchedule(RoutineViewer.this, routine);
                updatedRoutine.isActive(false);
                routineController.updateRoutine(updatedRoutine);
                Extensions.showToast(RoutineViewer.this,  getString(R.string.routine_disabled_message, routine.getName()), Toast.LENGTH_SHORT);
            }
        });

        headerLinearLayout.addView(routineActiveSwitch);

        mainLinearLayout.addView(headerLinearLayout);

        RecyclerView entryRecyclerView  = new RecyclerView(this);
        LinearLayout.LayoutParams entryRecyclerViewLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        entryRecyclerView.setLayoutParams(entryRecyclerViewLayoutParams);
        entryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration decoration = new DividerItemDecoration(RoutineViewer.this, 1);
        entryRecyclerView.addItemDecoration(decoration);

        entryRecyclerView.setAdapter(new RoutineRecyclerViewAdapter(RoutineViewer.this, getParent(),
                list, routine.getId()));
        entryRecyclerView.setVisibility(View.VISIBLE);

        mainLinearLayout.addView(entryRecyclerView);

        return mainLinearLayout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

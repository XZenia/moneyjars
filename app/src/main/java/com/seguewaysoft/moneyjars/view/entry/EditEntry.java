package com.seguewaysoft.moneyjars.view.entry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class EditEntry extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
{
    EditText descriptionEditText;
    EditText amountEditText;
    EditText dateEditText;

    EntryController entryController;

    Button confirmButton;

    Entry selectedEntry;

    Calendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_fields);

        entryController = new EntryController(EditEntry.this);

        selectedEntry = (Entry) getIntent().getExtras().get("selectedEntry");

        descriptionEditText = findViewById(R.id.DescriptionEditText);
        descriptionEditText.setText(selectedEntry.getDescription());

        amountEditText = findViewById(R.id.AmountEditText);
        amountEditText.setText(String.valueOf(selectedEntry.getAmount()));

        dateEditText = findViewById(R.id.DateEditText);
        dateEditText.setText(selectedEntry.getDate());

        calendar = Calendar.getInstance();

        dateEditText.setOnClickListener(v -> {
            try {
                Date date = Constants.dateFormat.parse(selectedEntry.getDate());
                calendar.setTime(date);
            } catch (ParseException e) {
                Extensions.showToast(EditEntry.this,"An error occurred! Please try again.", Toast.LENGTH_SHORT);
            }

            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                    EditEntry.this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            datePickerDialog.dismissOnPause(true);
            datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
        });

        confirmButton = findViewById(R.id.AddEntryConfirmButton);
        confirmButton.setOnClickListener(v -> ConfirmButtonClicked());

        getSupportActionBar().setTitle(getString(R.string.edit_entry));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void ConfirmButtonClicked(){
        //setup the page
        if (descriptionEditText.getText().toString().isEmpty() && amountEditText.getText().toString().isEmpty())
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Fields should not be empty!", Snackbar.LENGTH_SHORT);
        }
        else if (Double.parseDouble(amountEditText.getText().toString()) <= 0)
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Amount should be more than 0!", Snackbar.LENGTH_SHORT);
        }
        else
        {
            selectedEntry.setDate(dateEditText.getText().toString());
            selectedEntry.setDescription(descriptionEditText.getText().toString());
            selectedEntry.setAmount(Double.parseDouble(amountEditText.getText().toString()));
            entryController.updateEntry(selectedEntry);

            Extensions.showSnackbar(getCurrentFocus().getRootView(), getString(R.string.entry_update_success_message), Snackbar.LENGTH_SHORT);

            setResult(Constants.STATUS_OK);

            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        dateEditText.setText(Constants.dateFormat.format(calendar.getTime()));
    }
}

package com.seguewaysoft.moneyjars.view.jar;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.controller.JarController;

import com.seguewaysoft.moneyjars.view.entry.AddEntry;
import com.seguewaysoft.moneyjars.view.entry.AddIncomeEntry;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.model.Jar;
import com.seguewaysoft.moneyjars.view.routine.RoutineViewer;
import com.seguewaysoft.moneyjars.adapter.CardViewListAdapter;
import com.seguewaysoft.moneyjars.adapter.EntryRecyclerViewAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class JarViewer extends AppCompatActivity {

    final static String TAG = "JarViewer";

    private EntryController entryController;
    private JarController jarController;

    private FloatingActionButton fabMain;
    private FloatingActionButton fabAddMoney;
    private FloatingActionButton fabWithdrawMoney;

    private LinearLayout fabAddMoneyLayout;
    private LinearLayout fabWithdrawMoneyLayout;

    private RecyclerView cardViewRecyclerView;
    private TextView noEntriesTextView;

    boolean fabExpanded = false;
    int jarId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jar_viewer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        jarId = getIntent().getExtras().getInt("jarId");

        entryController = new EntryController(JarViewer.this);
        jarController = new JarController(JarViewer.this);

        fabMain = findViewById(R.id.Fab);
        fabAddMoney = findViewById(R.id.FabAddMoney);
        fabWithdrawMoney = findViewById(R.id.FabWithdrawMoney);

        fabMain.setOnClickListener(view -> {
            if (fabExpanded) {
                closeSubMenusFab();
            } else {
                openSubMenusFab();
            }
        });

        fabWithdrawMoney.setOnClickListener(v -> {
            Intent addEntry = new Intent(JarViewer.this, AddEntry.class);
            addEntry.putExtra("jarId", jarId);
            startActivityForResult(addEntry,1);
        });

        fabAddMoney.setOnClickListener(v -> {
            Intent addMoney = new Intent(JarViewer.this, AddIncomeEntry.class);
            addMoney.putExtra("jarId", jarId);
            startActivityForResult(addMoney, 1);
        });

        fabAddMoneyLayout = findViewById(R.id.LayoutAddMoney);
        fabWithdrawMoneyLayout = findViewById(R.id.LayoutWithdrawMoney);

        noEntriesTextView = findViewById(R.id.NoEntriesTextView);

        getSupportActionBar().setTitle(jarController.getJar(jarId).getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Close the Floating Action Button sub menu in case it is open.
        closeSubMenusFab();

        cardViewRecyclerView = findViewById(R.id.CardViewRecyclerView);
        cardViewRecyclerView.setHasFixedSize(true);
        cardViewRecyclerView.setItemViewCacheSize(20);
        cardViewRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LoadEntryListTask loadEntryListTask = new LoadEntryListTask();
        loadEntryListTask.execute();
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Constants.STATUS_OK)
        {
            recreate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_jar_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(JarViewer.this);
            dialog.setMessage(R.string.delete_jar_message);

            dialog.setPositiveButton(R.string.delete,
                    (deleteDialog, which) -> {
                        jarController.deleteJar(jarId);
                        entryController.deleteAllJarEntries(jarId);
                        finish();
                    }
            );

            dialog.setNegativeButton(R.string.cancel,
                    (cancelDialog, which) -> cancelDialog.cancel()
            );

            dialog.show();

            return true;
        }

        if (id == R.id.action_edit)
        {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(JarViewer.this);
            alertDialog.setMessage("Enter a new name for the jar");

            LayoutInflater inflater = getLayoutInflater();
            View inputFieldsView = inflater.inflate(R.layout.jar_input_fields, null);

            final Jar currentJar = jarController.getJar(jarId);

            final EditText name  = inputFieldsView.findViewById(R.id.JarNameEditText);
            name.setText(currentJar.getName());

            alertDialog.setPositiveButton(R.string.update,
                    (dialog, which) -> {
                        //setup the page
                        if (!name.getText().toString().isEmpty())
                        {
                            currentJar.setName(name.getText().toString());

                            jarController.updateJar(jarId, currentJar);
                            recreate();

                            Extensions.showToast(JarViewer.this, "Jar name updated!", Toast.LENGTH_SHORT);
                        }
                        else
                        {
                            Extensions.showToast(JarViewer.this, "Fields must not be empty!", Toast.LENGTH_SHORT);
                        }
                    }
            );

            alertDialog.setNegativeButton(R.string.cancel,
                    (dialog, which) -> dialog.cancel()
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        }

        if (id == R.id.routines)
        {
            Intent addEntry = new Intent(JarViewer.this, RoutineViewer.class);
            addEntry.putExtra("jarId", jarId);
            startActivityForResult(addEntry,1);
        }

        if (id == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupContentList()
    {
        List<Entry> entries = entryController.getAllEntries(jarId,false);

        if (!entries.isEmpty())
        {
            List<String> dates = entryController.getAllDates(jarId);
            List<LinearLayout> linearLayouts = new ArrayList<>();
            for (String date: dates)
            {
                ArrayList<Entry> entryDates = new ArrayList<>();
                for(Entry entry: entries)
                {
                    if (entry.getDate().contentEquals(date))
                    {
                        entryDates.add(entry);
                    }
                }
                linearLayouts.add(createRecyclerViewElement(entryDates, date));
            }

            CardViewListAdapter cardViewListAdapter = new CardViewListAdapter(linearLayouts);
            cardViewRecyclerView.setAdapter(cardViewListAdapter);
        }
        else
        {
            cardViewRecyclerView.setVisibility(View.GONE);
            noEntriesTextView.setVisibility(View.VISIBLE);
        }
    }

    private LinearLayout createRecyclerViewElement(ArrayList<Entry> list, String date)
    {
        EntryRecyclerViewAdapter entryRecyclerViewAdapter = new EntryRecyclerViewAdapter(JarViewer.this, list, "EntryViewer");

        LinearLayout linearLayout = new LinearLayout(JarViewer.this);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(linearLayoutParams);

        TextView entryListLabel = new TextView(JarViewer.this);
        LinearLayout.LayoutParams entryListLabelLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        entryListLabelLayoutParams.gravity = Gravity.CENTER_VERTICAL;
        entryListLabel.setLayoutParams(entryListLabelLayoutParams);
        entryListLabel.setPadding(10,10,10,10);
        entryListLabel.setTextColor(getResources().getColor(R.color.colorText, getTheme()));

        entryListLabel.setText(date);

        linearLayout.addView(entryListLabel);

        RecyclerView entryRecyclerView  = new RecyclerView(this);
        LinearLayout.LayoutParams entryRecyclerViewLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        entryRecyclerView.setLayoutParams(entryRecyclerViewLayoutParams);
        entryRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        DividerItemDecoration decoration = new DividerItemDecoration(JarViewer.this, 1);
        entryRecyclerView.addItemDecoration(decoration);

        entryRecyclerView.setAdapter(entryRecyclerViewAdapter);
        entryRecyclerView.setVisibility(View.VISIBLE);

        entryRecyclerView.setHasFixedSize(true);
        entryRecyclerView.setItemViewCacheSize(20);

        linearLayout.addView(entryRecyclerView);

        return linearLayout;
    }

    private void closeSubMenusFab()
    {
        fabAddMoneyLayout.setVisibility(View.GONE);
        fabWithdrawMoneyLayout.setVisibility(View.GONE);
        fabMain.setImageResource(R.drawable.ic_action_new);

        fabExpanded = false;
    }

    private void openSubMenusFab()
    {
        fabAddMoneyLayout.setVisibility(View.VISIBLE);
        if (entryController.getTotalJarContent(jarId) > 0)
        {
            fabWithdrawMoneyLayout.setVisibility(View.VISIBLE);
        }
        //Change settings icon to 'X' icon
        fabMain.setImageResource(R.drawable.ic_action_cancel);

        fabExpanded = true;
    }

    private class LoadEntryListTask extends AsyncTask<String, String, String>
    {
        ProgressBar progressBar = findViewById(R.id.JarViewerLoadingBar);
        @Override
        protected String doInBackground(String... strings) {
            runOnUiThread(() -> setupContentList());
            return String.valueOf(Constants.STATUS_OK);
        }
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setVisibility(View.VISIBLE);
            cardViewRecyclerView.setVisibility(View.GONE);
        }

        protected void onPostExecute(Long result) {
            progressBar.setVisibility(View.GONE);
            if (noEntriesTextView.getVisibility() == View.GONE)
            {
                cardViewRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }
}

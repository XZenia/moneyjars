package com.seguewaysoft.moneyjars.view.entry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.controller.UnallocatedMoneyController;
import com.seguewaysoft.moneyjars.model.Entry;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddIncomeEntry extends AppCompatActivity
{
    TextView totalIncomeTextView;
    EditText amount;

    EntryController entryController;

    Button confirmButton;

    int jarId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_income_entry);

        jarId = getIntent().getExtras().getInt("jarId");

        entryController = new EntryController(AddIncomeEntry.this);

        amount = findViewById(R.id.AmountEditText);

        confirmButton = findViewById(R.id.AddEntryConfirmButton);

        confirmButton.setOnClickListener(v -> ConfirmButtonClicked());

        totalIncomeTextView = findViewById(R.id.MoneyLeftTextView);

        double unallocatedMoneyAmount = UnallocatedMoneyController.getUnallocatedMoney(AddIncomeEntry.this);

        totalIncomeTextView.setText(getString(R.string.total_income_add_income_entry, String.format("%.2f", unallocatedMoneyAmount)));

        getSupportActionBar().setTitle(getString(R.string.add_entry));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void ConfirmButtonClicked()
    {
        if (amount.getText().toString().isEmpty())
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Fields should not be empty!", Snackbar.LENGTH_SHORT);
        }
        else if(Double.parseDouble(amount.getText().toString()) <= 0)
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Amount should be more than 0!", Snackbar.LENGTH_SHORT);
        }
        else if (Double.parseDouble(amount.getText().toString()) > UnallocatedMoneyController.getUnallocatedMoney(AddIncomeEntry.this))
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Amount should not exceed your set total income!", Snackbar.LENGTH_SHORT);
        }
        else
        {
            Entry newEntry = new Entry();

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM / dd / yyyy - EEEE", Locale.ENGLISH);

            newEntry.setJarId(jarId);
            newEntry.setAmount(Double.parseDouble(amount.getText().toString()));
            newEntry.setDescription(getString(R.string.income_entry_default_description));

            newEntry.setCategory(CategoryController.income.getId());

            newEntry.setDate(String.valueOf(dateFormat.format(calendar.getTime())));

            entryController.addEntry(newEntry);

            setResult(Constants.STATUS_OK);

            UnallocatedMoneyController.transferUnallocatedMoneyToJar(AddIncomeEntry.this,newEntry.getAmount());

            clearFields();

            double unallocatedMoneyAmount = UnallocatedMoneyController.getUnallocatedMoney(AddIncomeEntry.this);
            totalIncomeTextView.setText(getString(R.string.total_income_add_income_entry, String.format("%.2f", unallocatedMoneyAmount)));

            Extensions.showSnackbar(getCurrentFocus().getRootView(), getString(R.string.entry_added_message), Snackbar.LENGTH_SHORT);
        }
    }

    private void clearFields(){
        amount.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}

package com.seguewaysoft.moneyjars.view.routine;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.RoutineExtensions;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.controller.RoutineController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.model.Routine;
import com.seguewaysoft.moneyjars.adapter.RoutineEntryRecyclerViewAdapter;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class EditRoutine extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    EditText routineNameEditText;

    EditText timeEditTextDisplay;
    EditText timeEditText;

    MaterialDayPicker materialDayPicker;
    Button addEntriesButton;
    RecyclerView entryRecyclerView;
    TextView noEntriesTextView;

    TimePickerDialog timePickerDialog;
    ArrayList<Entry> entryList;

    RoutineController routineController;

    Routine selectedRoutine;

    int routineId;

    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_routine);

        routineId = getIntent().getExtras().getInt("routineId");

        routineController = new RoutineController(EditRoutine.this);
        selectedRoutine = routineController.getRoutine(routineId);

        entryList = new ArrayList<>(selectedRoutine.getEntries());

        routineNameEditText = findViewById(R.id.RoutineNameEditText);
        routineNameEditText.setText(selectedRoutine.getName());

        timeEditTextDisplay = findViewById(R.id.RoutineTimeEditTextDisplay);
        timeEditText = findViewById(R.id.RoutineTimeEditText);

        materialDayPicker = findViewById(R.id.RoutineDayPicker);
        materialDayPicker.setSelectedDays(selectedRoutine.getDays());

        entryRecyclerView = findViewById(R.id.CardViewRecyclerView);
        entryRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        noEntriesTextView = findViewById(R.id.NoEntriesTextView);

        calendar = Extensions.convertStringToTime(EditRoutine.this, selectedRoutine.getTime());

        timeEditTextDisplay.setText(Extensions.convertTimeToString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true));
        timeEditText.setText(Extensions.convertTimeToString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false));

        timeEditTextDisplay.setOnClickListener(v -> {
            calendar = Extensions.convertStringToTime(EditRoutine.this, timeEditText.getText().toString());

            timePickerDialog = TimePickerDialog.newInstance(
                    EditRoutine.this,
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    false
            );
            timePickerDialog.dismissOnPause(true);
            timePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
        });

        addEntriesButton = findViewById(R.id.AddEntriesButton);
        addEntriesButton.setOnClickListener(view -> {
            //create the dialog to be shown when the button gets clicked
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditRoutine.this);
            alertDialog.setMessage("Enter an entry for the Routine");

            LayoutInflater inflater = getLayoutInflater();
            View inputFieldsView = inflater.inflate(R.layout.routine_entry_inflater_fields, null);

            final EditText descriptionEditText = inputFieldsView.findViewById(R.id.DescriptionEditText);
            final EditText amountEditText = inputFieldsView.findViewById(R.id.AmountEditText);

            alertDialog.setPositiveButton(R.string.add,
                    (dialog, which) -> {
                        if (descriptionEditText.getText().toString().isEmpty())
                        {
                            Extensions.showSnackbar(view,"Description text field must not be empty!", Snackbar.LENGTH_SHORT);
                        }
                        else if (amountEditText.getText().toString().isEmpty())
                        {
                            Extensions.showSnackbar(view,"Amount text field must not be empty!", Snackbar.LENGTH_SHORT);
                        }
                        else
                        {
                            double amount = Double.parseDouble(amountEditText.getText().toString());
                            String description = descriptionEditText.getText().toString();

                            Entry newEntry = new Entry();
                            newEntry.setDescription(description);
                            newEntry.setAmount(amount);
                            newEntry.setJarId(selectedRoutine.getJarId());
                            newEntry.setCategory(CategoryController.expense.getId());
                            entryList.add(newEntry);

                            loadList();

                            Extensions.showSnackbar(view, "Entry added!", Snackbar.LENGTH_SHORT);
                        }

                    });

            alertDialog.setNegativeButton(R.string.cancel,
                    (dialog, which) -> dialog.cancel()
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        });

        getSupportActionBar().setTitle("Edit Routine");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadList();
    }

    private void loadList()
    {
        RoutineEntryRecyclerViewAdapter adapter = new RoutineEntryRecyclerViewAdapter(EditRoutine.this, entryList);

        if (!entryList.isEmpty())
        {
            entryRecyclerView.setAdapter(adapter);

            DividerItemDecoration decoration = new DividerItemDecoration(EditRoutine.this, 1);
            entryRecyclerView.addItemDecoration(decoration);

            entryRecyclerView.setVisibility(View.VISIBLE);
            noEntriesTextView.setVisibility(View.GONE);
        }
        else
        {
            entryRecyclerView.setVisibility(View.GONE);
            noEntriesTextView.setVisibility(View.VISIBLE);
        }
    }
    
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time = Extensions.convertTimeToString(hourOfDay, minute, true);
        timeEditTextDisplay.setText(time);
        timeEditText.setText(Extensions.convertTimeToString(hourOfDay, minute, false));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_routine, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == android.R.id.home)
        {
            finish();
        }
        else if (id == R.id.action_save)
        {
            if (routineNameEditText.getText().toString().isEmpty())
            {
                Extensions.showSnackbar(getCurrentFocus().getRootView(), "Routine name field should not be empty!", Snackbar.LENGTH_SHORT);
            }
            else if (timeEditTextDisplay.getText().toString().isEmpty())
            {
                Extensions.showSnackbar(getCurrentFocus().getRootView(), "Routine name field should not be empty!", Snackbar.LENGTH_SHORT);
            }
            else if (materialDayPicker.getSelectedDays().isEmpty())
            {
                Extensions.showSnackbar(getCurrentFocus().getRootView(), "You must select the days when the routine will apply your entries.", Snackbar.LENGTH_SHORT);
            }
            else if (entryList.isEmpty())
            {
                Extensions.showSnackbar(getCurrentFocus().getRootView(), "You must add entries to the routine.", Snackbar.LENGTH_SHORT);
            }
            else
            {
                selectedRoutine.setName(routineNameEditText.getText().toString());
                selectedRoutine.setTime(timeEditText.getText().toString());
                selectedRoutine.setDays(materialDayPicker.getSelectedDays());
                selectedRoutine.setEntries(entryList);

                routineController.updateRoutine(selectedRoutine);

                if (selectedRoutine.isActive()){
                    RoutineExtensions.removeRoutineSchedule(EditRoutine.this, selectedRoutine);
                    Routine updatedRoutine = RoutineExtensions.scheduleRoutine(EditRoutine.this, selectedRoutine);
                    routineController.updateRoutine(updatedRoutine);

                    Extensions.showToast(getApplicationContext(), "Routine rescheduled!", Toast.LENGTH_SHORT);
                    Extensions.showToast(getApplicationContext(), "Routine rescheduled!", Toast.LENGTH_SHORT);
                }
                
                finish();

                Extensions.showSnackbar(getCurrentFocus().getRootView(), "Routine added to jar!", Snackbar.LENGTH_SHORT);
            }
        }
        else if (id == R.id.action_delete)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(EditRoutine.this);
            dialog.setMessage("Are you sure you want to delete this routine?");

            dialog.setPositiveButton(R.string.delete,
                    (deleteDialog, which) -> {
                        routineController.deleteRoutine(selectedRoutine.getId());
                        finish();
                    }
            );

            dialog.setNegativeButton(R.string.cancel,
                    (cancelDialog, which) -> cancelDialog.cancel()
            );
            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

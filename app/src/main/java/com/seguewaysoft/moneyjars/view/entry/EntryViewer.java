package com.seguewaysoft.moneyjars.view.entry;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.controller.JarController;
import com.seguewaysoft.moneyjars.model.Entry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class EntryViewer extends AppCompatActivity
{
    final static String TAG = "EntryViewer";

    EntryController entryController;

    TextView descriptionTextView;
    TextView amountTextView;
    TextView categoryTextView;
    TextView dateTextView;

    int entryId;

    Entry selectedEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_viewer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.Fab);
        fab.setOnClickListener(view -> {
            Intent editEntry = new Intent(EntryViewer.this, EditEntry.class);
            editEntry.putExtra("selectedEntry", selectedEntry);
            startActivityForResult(editEntry, Constants.STATUS_OK);
        });

        descriptionTextView = findViewById(R.id.DescriptionTextView);
        amountTextView = findViewById(R.id.AmountTextView);
        categoryTextView = findViewById(R.id.CategoryTextView);
        dateTextView = findViewById(R.id.DateTextView);

        entryController = new EntryController(EntryViewer.this);

        try{
            entryId = getIntent().getExtras().getInt("entryId");
        } catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            finish();
        }

        loadEntry(entryId);

        JarController jarController = new JarController(EntryViewer.this);
        try{
            getSupportActionBar().setTitle(jarController.getJar(selectedEntry.getJarId()).getName());
        } catch (Exception ex){
            Log.e(TAG, ex.getMessage());
            finish();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void loadEntry(int entryId)
    {
        selectedEntry = entryController.getEntry(entryId);

        String category = selectedEntry.getCategory() == CategoryController.income.getId() ? "Income" : "Expense";

        descriptionTextView.setText(selectedEntry.getDescription());
        amountTextView.setText(getString(R.string.amount_with_input, String.format("%.2f", selectedEntry.getAmount())));
        categoryTextView.setText(getString(R.string.category_entry_viewer, category));
        dateTextView.setText(getString(R.string.date_entry_viewer, selectedEntry.getDate()));
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_entry_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(EntryViewer.this);
            dialog.setTitle("Are you sure you want to delete this entry?");

            dialog.setPositiveButton("Delete",
                    (dialog1, which) -> {
                        entryController.deleteEntry(entryId);
                        setResult(Constants.STATUS_OK);
                        finish();
                    }
            );

            dialog.setNegativeButton("Cancel",
                    (dialog12, which) -> dialog12.cancel()
            );

            dialog.show();
            return true;
        }
        else if (id == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

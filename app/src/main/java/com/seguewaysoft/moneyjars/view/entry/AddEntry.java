package com.seguewaysoft.moneyjars.view.entry;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

public class AddEntry extends AppCompatActivity implements DatePickerDialog.OnDateSetListener
{
    EditText descriptionEditText;
    EditText amountEditText;
    EditText dateEditText;

    EntryController entryController;

    Button confirmButton;

    Calendar calendar;

    int jarId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_fields);

        jarId = getIntent().getExtras().getInt("jarId");

        entryController = new EntryController(AddEntry.this);

        descriptionEditText = findViewById(R.id.DescriptionEditText);
        amountEditText = findViewById(R.id.AmountEditText);
        dateEditText = findViewById(R.id.DateEditText);

        dateEditText.setText(Constants.dateFormat.format(Calendar.getInstance().getTime()));
        calendar = Calendar.getInstance();

        dateEditText.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                    AddEntry.this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            );
            datePickerDialog.dismissOnPause(true);
            datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
        });

        confirmButton = findViewById(R.id.AddEntryConfirmButton);

        confirmButton.setOnClickListener(v -> ConfirmButtonClicked());

        getSupportActionBar().setTitle(getString(R.string.add_entry));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void ConfirmButtonClicked(){
        //setup the page
        if (descriptionEditText.getText().toString().isEmpty() || amountEditText.getText().toString().isEmpty())
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Fields should not be empty!", Snackbar.LENGTH_SHORT);
        }
        else if (Double.parseDouble(amountEditText.getText().toString()) <= 0)
        {
            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Amount should be more than 0!", Snackbar.LENGTH_SHORT);
        }
        else
        {
            Entry newEntry = new Entry();
            newEntry.setJarId(jarId);
            newEntry.setAmount(Double.parseDouble(amountEditText.getText().toString()));
            newEntry.setDescription(descriptionEditText.getText().toString());
            newEntry.setDate(dateEditText.getText().toString());
            newEntry.setCategory(CategoryController.expense.getId());

            entryController.addEntry(newEntry);

            setResult(Constants.STATUS_OK);

            clearFields();

            Extensions.showSnackbar(getCurrentFocus().getRootView(), "Entry has been added to the jar!", Snackbar.LENGTH_SHORT);
        }
    }

    private void clearFields(){
        descriptionEditText.setText("");
        amountEditText.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        dateEditText.setText(Constants.dateFormat.format(calendar.getTime()));
    }
}
package com.seguewaysoft.moneyjars.model;

import java.io.Serializable;
import java.util.List;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class Routine implements Serializable {

    private int id;
    private String name;
    private List<Entry> entries;
    private int jarId;
    private List<MaterialDayPicker.Weekday> days;
    private String time;
    private int alarmId;
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public int getJarId() {
        return jarId;
    }

    public void setJarId(int jarId) {
        this.jarId = jarId;
    }

    public List<MaterialDayPicker.Weekday>  getDays() {
        return days;
    }

    public void setDays(List<MaterialDayPicker.Weekday>  days) {
        this.days = days;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isActive() {
        return isActive;
    }

    public int getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(int alarmId) {
        this.alarmId = alarmId;
    }

    public void isActive(boolean active) {
        isActive = active;
    }
}

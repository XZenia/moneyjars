package com.seguewaysoft.moneyjars;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.controller.JarController;
import com.seguewaysoft.moneyjars.controller.UnallocatedMoneyController;
import com.seguewaysoft.moneyjars.model.Jar;
import com.seguewaysoft.moneyjars.adapter.JarRecyclerViewAdapter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private RecyclerView jarRecyclerView;
    private JarController jarController;

    private TextView moneyLeftTextView;
    private TextView unallocatedMoneyTextView;

    private LinearLayout fabAddIncomeLayout;
    private LinearLayout fabAddJarLayout;

    private TextView noJarMessageTextView;

    private FloatingActionButton fabMain;

    private boolean fabExpanded = false;

    private static final String FIRST_RUN = "first_run";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        jarController = new JarController(MainActivity.this);

        setupLayout();

        CategoryController.setCategories();

        loadList();
        calculateTotalValues();

        //Setting up the options in the initial app run.
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean(FIRST_RUN, true))
        {
            //Set initial values in the SharedPreferences.
            editor.putFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, 0);
            editor.putBoolean(FIRST_RUN, false);
            editor.apply();
        }

        //Close the Floating Action Button sub menu in case it is open.
        closeSubMenusFab();
    }

    private void setupLayout()
    {
        fabAddIncomeLayout = findViewById(R.id.LayoutAddTotalIncome);
        fabAddJarLayout = findViewById(R.id.LayoutFabAddJar);

        fabMain = findViewById(R.id.Fab);

        FloatingActionButton fabAddJar = findViewById(R.id.FabAddJar);
        FloatingActionButton fabAddIncome = findViewById(R.id.FabAddIncome);

        fabMain.setOnClickListener(view -> {
            if (fabExpanded)
            {
                closeSubMenusFab();
            }
            else
            {
                openSubMenusFab();
            }
        });

        fabAddJar.setOnClickListener(view -> {
            //create the dialog to be shown when the button gets clicked
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setMessage(R.string.jar_name_message);
            LayoutInflater inflater = getLayoutInflater();
            View inputFieldsView = inflater.inflate(R.layout.jar_input_fields, null);

            final EditText input = inputFieldsView.findViewById(R.id.JarNameEditText);

            alertDialog.setPositiveButton(R.string.add,
                    (dialog, which) -> {
                        //setup the page
                        if (!input.getText().toString().isEmpty())
                        {
                            Jar newJar = new Jar();
                            newJar.setName(input.getText().toString());

                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM / dd / yyyy - EEEE", Locale.ENGLISH);
                            newJar.setDate(String.valueOf(dateFormat.format(calendar.getTime())));

                            jarController.addJar(newJar);

                            recreate();

                            Extensions.showSnackbar(view, getString(R.string.jar_added_message), Snackbar.LENGTH_SHORT);
                        }
                        else
                        {
                            Extensions.showSnackbar(view, getString(R.string.jar_name_required_message), Snackbar.LENGTH_SHORT);
                        }
                    }
            );

            alertDialog.setNegativeButton(R.string.cancel,
                    (dialog, which) -> dialog.cancel()
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        });

        fabAddIncome.setOnClickListener(view -> {
            //create the dialog to be shown when the button gets clicked
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setMessage("Enter the amount of income you earned");

            LayoutInflater inflater = getLayoutInflater();
            View inputFieldsView = inflater.inflate(R.layout.total_amount_input_fields, null);

            final EditText input = inputFieldsView.findViewById(R.id.TotalAmountEditText);

            alertDialog.setPositiveButton(R.string.add,
                    (dialog, which) -> {
                        //setup the page
                        if (!input.getText().toString().isEmpty())
                        {
                            double totalAmountInput = Double.parseDouble(input.getText().toString());
                            UnallocatedMoneyController.addUnallocatedMoney(MainActivity.this, totalAmountInput);

                            recreate();

                            Extensions.showSnackbar(view, "Money added to the Total Income jar!", Snackbar.LENGTH_SHORT);
                        }
                        else
                        {
                            Extensions.showSnackbar(view, "Amount text field should not be empty!", Snackbar.LENGTH_SHORT);
                        }
                    });

            alertDialog.setNegativeButton(R.string.cancel,
                    (dialog, which) -> dialog.cancel()
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        });

        getSupportActionBar().setTitle(getString(R.string.my_jars));

        jarRecyclerView = findViewById(R.id.JarListViewer);
        jarRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        unallocatedMoneyTextView = findViewById(R.id.UnallocatedMoneyTextView);
        unallocatedMoneyTextView.setOnClickListener(view -> {
            //create the dialog to be shown when the button gets clicked
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setMessage(R.string.edit_unallocated_money_label);

            LayoutInflater inflater = getLayoutInflater();
            View inputFieldsView = inflater.inflate(R.layout.total_amount_input_fields, null);

            final EditText input = inputFieldsView.findViewById(R.id.TotalAmountEditText);
            input.setText(String.valueOf(UnallocatedMoneyController.getUnallocatedMoney(MainActivity.this)));

            alertDialog.setPositiveButton(R.string.update,
                    (dialog, which) -> {
                        //setup the page
                        if (!input.getText().toString().isEmpty())
                        {
                            double totalAmountInput = Double.parseDouble(input.getText().toString());
                            UnallocatedMoneyController.editUnallocatedMoney(MainActivity.this, totalAmountInput);

                            recreate();

                            Extensions.showSnackbar(view, getString(R.string.money_added_to_total_income_jar_message), Snackbar.LENGTH_SHORT);
                        }
                        else
                        {
                            Extensions.showSnackbar(view, getString(R.string.amount_text_field_empty_message), Snackbar.LENGTH_SHORT);
                        }
                    });

            alertDialog.setNegativeButton(R.string.cancel,
                    (dialog, which) -> dialog.cancel()
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        });

        moneyLeftTextView = findViewById(R.id.MoneyLeftTextView);

        noJarMessageTextView = findViewById(R.id.NoJarMessageTextView);

        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), 1);
        jarRecyclerView.addItemDecoration(decoration);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        loadList();
        calculateTotalValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void loadList()
    {
        List<Jar> jarList = jarController.getAllJars();
        JarRecyclerViewAdapter adapter = new JarRecyclerViewAdapter(MainActivity.this, jarList);
        if (!jarList.isEmpty())
        {
            jarRecyclerView.setAdapter(adapter);
            jarRecyclerView.setVisibility(View.VISIBLE);

            noJarMessageTextView.setVisibility(View.GONE);
        }
        else
        {
            jarRecyclerView.setVisibility(View.GONE);
            noJarMessageTextView.setVisibility(View.VISIBLE);
        }
    }

    private void calculateTotalValues()
    {
        double unallocatedMoney = UnallocatedMoneyController.getUnallocatedMoney(MainActivity.this);
        double remainingAllocatedMoney = Extensions.getRemainingAllocatedMoney(MainActivity.this);

        unallocatedMoneyTextView.setText(getString(R.string.unallocated_money, String.format(Locale.getDefault(), "%.2f", unallocatedMoney)));
        moneyLeftTextView.setText(getString(R.string.money_left, String.format(Locale.getDefault(), "%.2f", remainingAllocatedMoney)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void closeSubMenusFab()
    {
        fabAddJarLayout.setVisibility(View.INVISIBLE);
        fabAddIncomeLayout.setVisibility(View.INVISIBLE);
        fabMain.setImageResource(R.drawable.ic_action_new);

        fabExpanded = false;
    }

    private void openSubMenusFab()
    {
        fabAddJarLayout.setVisibility(View.VISIBLE);
        fabAddIncomeLayout.setVisibility(View.VISIBLE);
        //Change settings icon to 'X' icon
        fabMain.setImageResource(R.drawable.ic_action_cancel);

        fabExpanded = true;
    }
}

package com.seguewaysoft.moneyjars.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.model.Routine;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class RoutineController extends SQLiteOpenHelper {

    private static final String TAG = "RoutineController";
    private static final String TABLENAME = "routine";
    private static final String COLID = "_ID";
    private static final String COL1 = "name";
    private static final String COL2 = "entries";
    private static final String COL3 = "jar";
    private static final String COL4 = "date";
    private static final String COL5 = "time";
    private static final String COL6 = "status";
    private static final String COL7 = "alarmId";

    private static final int DATABASEVERSION = 10;

    public RoutineController(Context context){
        super(context,TABLENAME,null,DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String createTable = "CREATE TABLE "+ TABLENAME + "(_ID INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, "+
                COL1 +" TEXT, "+ COL2 +" TEXT, "+ COL3 +" TEXT, "+ COL4 +" TEXT, "
                + COL5 +" TEXT, "+ COL6 +" INTEGER, " + COL7 +" TEXT);";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLENAME);
        onCreate(db);
    }

    public void addRoutine(Routine newRoutine)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        Gson gson = new Gson();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, newRoutine.getName());
        contentValues.put(COL2, gson.toJson(newRoutine.getEntries()));
        contentValues.put(COL3, newRoutine.getJarId());
        contentValues.put(COL4, gson.toJson(newRoutine.getDays()));
        contentValues.put(COL5, newRoutine.getTime());
        contentValues.put(COL6, newRoutine.isActive());

        db.insert(TABLENAME,null,contentValues);
    }

    public void updateRoutine(Routine updatedRoutine)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Gson gson = new Gson();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, updatedRoutine.getName());
        contentValues.put(COL2, gson.toJson(updatedRoutine.getEntries()));
        contentValues.put(COL4, gson.toJson(updatedRoutine.getDays()));
        contentValues.put(COL5, updatedRoutine.getTime());
        contentValues.put(COL6, updatedRoutine.isActive());
        contentValues.put(COL7, updatedRoutine.getAlarmId());

        db.update(TABLENAME,contentValues,COLID+" = ?",new String[]{Integer.toString(updatedRoutine.getId())});
    }

    public Routine getRoutine(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME + " WHERE " + COLID + " = " + id;
        Cursor data = db.rawQuery(query,null);

        Routine retrievedRoutine = new Routine();
        Gson gson = new Gson();

        if (data.moveToFirst())
        {
            do {
                retrievedRoutine.setId(data.getInt(0));
                retrievedRoutine.setName(data.getString(1));
                retrievedRoutine.setEntries(gson.fromJson(data.getString(2), new TypeToken<List<Entry>>(){}.getType()));
                retrievedRoutine.setJarId(data.getInt(3));
                retrievedRoutine.setDays(gson.fromJson(data.getString(4), new TypeToken<List<MaterialDayPicker.Weekday>>(){}.getType()));
                retrievedRoutine.setTime(data.getString(5));
                retrievedRoutine.isActive(data.getInt(6) == 1);
                retrievedRoutine.setAlarmId(data.getInt(7));
            } while(data.moveToNext());
        }

        data.close();

        return retrievedRoutine;
    }

    public ArrayList<Routine> getJarRoutines(int jarId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME + " WHERE " + COL3 + " = " + jarId;
        Cursor data = db.rawQuery(query,null);

        Gson gson = new Gson();

        ArrayList<Routine> routines = new ArrayList<>();

        if (data.moveToFirst())
        {
            do {
                Routine retrievedRoutine = new Routine();
                retrievedRoutine.setId(data.getInt(0));
                retrievedRoutine.setName(data.getString(1));
                retrievedRoutine.setEntries(gson.fromJson(data.getString(2), new TypeToken<List<Entry>>(){}.getType()));
                retrievedRoutine.setJarId(data.getInt(3));
                retrievedRoutine.setDays(gson.fromJson(data.getString(4),  new TypeToken<List<MaterialDayPicker.Weekday>>(){}.getType()));
                retrievedRoutine.setTime(data.getString(5));
                retrievedRoutine.isActive(data.getInt(6) == 1);
                retrievedRoutine.setAlarmId(data.getInt(7));
                routines.add(retrievedRoutine);
            } while(data.moveToNext());
        }

        data.close();

        return routines;
    }

    public void deleteRoutine(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLENAME, COLID + " = ?", new String[]{String.valueOf(id)});
    }

    public void changeRoutineStatus(int routineId, boolean routineStatus)
    {
        try
        {
            Routine selectedRoutine = getRoutine(routineId);
            selectedRoutine.isActive(routineStatus);
            updateRoutine(selectedRoutine);
        }
        catch(Exception exception)
        {
            Log.e(TAG, exception.getMessage());
        }
    }

    public ArrayList<Routine> getActiveRoutines()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLENAME + " WHERE " + COL6 + " = 1";
        Cursor data = db.rawQuery(query,null);
        Gson gson = new Gson();

        ArrayList<Routine> routines = new ArrayList<>();

        if (data.moveToFirst())
        {
            do {
                Routine retrievedRoutine = new Routine();
                retrievedRoutine.setId(data.getInt(0));
                retrievedRoutine.setName(data.getString(1));
                retrievedRoutine.setEntries(gson.fromJson(data.getString(2), new TypeToken<List<Entry>>(){}.getType()));
                retrievedRoutine.setJarId(data.getInt(3));
                retrievedRoutine.setDays(gson.fromJson(data.getString(4),  new TypeToken<List<MaterialDayPicker.Weekday>>(){}.getType()));
                retrievedRoutine.setTime(data.getString(5));
                retrievedRoutine.isActive(data.getInt(6) == 1);
                retrievedRoutine.setAlarmId(data.getInt(7));
                routines.add(retrievedRoutine);
            } while(data.moveToNext());
        }

        data.close();

        return routines;
    }
}
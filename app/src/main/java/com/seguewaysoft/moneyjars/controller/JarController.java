package com.seguewaysoft.moneyjars.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.seguewaysoft.moneyjars.model.Jar;

import java.util.ArrayList;
import java.util.List;

public class JarController extends SQLiteOpenHelper
{
    private static final String TAG = "JarController";
    private static final String TABLENAME = "jar";
    private static final String COLID = "_ID";
    private static final String COL1 = "name";
    private static final String COL2 = "date";

    private static final int DATABASEVERSION = 3;

    public JarController(Context context)
    {
        super(context,TABLENAME,null,DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String createTable = "CREATE TABLE "+ TABLENAME + "(_ID INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, "+
                COL1 +" TEXT, "+ COL2 +" TEXT);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLENAME);
        onCreate(db);
    }

    public long addJar(Jar newJar)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, newJar.getName());
        contentValues.put(COL2, newJar.getDate());

        Log.d(TAG, "addJar: Adding "+ newJar.getName() + " to "+ TABLENAME);
        return db.insert(TABLENAME,null,contentValues);
    }

    public void updateJar(int id, Jar editedJar)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, editedJar.getName());
        db.update(TABLENAME,contentValues,COLID+" = ?",new String[]{Integer.toString(id)});
    }

    public void deleteJar(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLENAME, COLID + " = ?", new String[]{String.valueOf(id)});
    }

    public List<Jar> getAllJars()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME;
        Cursor data = db.rawQuery(query,null);

        List<Jar> jarList = new ArrayList<>();

        while(data.moveToNext())
        {
            Jar retrievedJar = new Jar();
            retrievedJar.setId(data.getInt(0));
            retrievedJar.setName(data.getString(1));
            jarList.add(retrievedJar);
        }

        data.close();

        return jarList;
    }

    public Jar getJar(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLENAME + " WHERE "+ COLID + " = " + id;
        Cursor data = db.rawQuery(query,null);

        Jar retrievedJar = new Jar();

        if (data.moveToFirst())
        {
            do {
                retrievedJar.setId(data.getInt(0));
                retrievedJar.setName(data.getString(1));
            } while (data.moveToNext());
        }

        data.close();
        return retrievedJar;
     }
}

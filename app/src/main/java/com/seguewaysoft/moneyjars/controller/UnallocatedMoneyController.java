package com.seguewaysoft.moneyjars.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UnallocatedMoneyController {

    public static String UnallocatedMoneyKeyName = "unallocated_money";

    public static void addUnallocatedMoney(Context context, double amount)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        double jarAmount = sharedPreferences.getFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, 0);

        editor.putFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, Float.parseFloat(String.valueOf(jarAmount + amount)));
        editor.apply();
    }

    public static void transferUnallocatedMoneyToJar(Context context, double amount)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        double jarAmount = sharedPreferences.getFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, 0);

        editor.putFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, Float.parseFloat(String.valueOf(jarAmount - amount)));
        editor.apply();
    }

    public static void editUnallocatedMoney(Context context, double amount)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, Float.parseFloat(String.valueOf(amount)));
        editor.apply();
    }

    public static double getUnallocatedMoney(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getFloat(UnallocatedMoneyController.UnallocatedMoneyKeyName, 0);
    }
}

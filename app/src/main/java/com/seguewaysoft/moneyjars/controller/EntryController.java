package com.seguewaysoft.moneyjars.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seguewaysoft.moneyjars.model.Entry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EntryController extends SQLiteOpenHelper {

    private static final String TAG = "EntryController";
    private static final String TABLENAME = "entry";
    private static final String COLID = "_ID";
    private static final String COL1 = "name";
    private static final String COL2 = "amount";
    private static final String COL3 = "category";
    private static final String COL4 = "date";
    private static final String COL5 = "time";
    private static final String COL6 = "jarID";

    private static final int DATABASEVERSION = 2;

    public EntryController(Context context){
        super(context,TABLENAME,null,DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String createTable = "CREATE TABLE "+ TABLENAME + "(_ID INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, "+
                COL1 +" TEXT, "+ COL2 +" TEXT, "+ COL3 +" TEXT, "+ COL4 +" TEXT, "+ COL5 +" TEXT, "+ COL6 + " TEXT);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLENAME);
        onCreate(db);
    }

    public void addEntry(Entry newEntry)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, newEntry.getDescription());
        contentValues.put(COL2, newEntry.getAmount());
        contentValues.put(COL3, newEntry.getCategory());
        contentValues.put(COL4, newEntry.getDate());
        contentValues.put(COL5, String.valueOf(Calendar.getInstance().getTime()));
        contentValues.put(COL6, newEntry.getJarId());

        db.insert(TABLENAME,null,contentValues);
    }

    public void updateEntry(Entry editedEntry)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, editedEntry.getDescription());
        contentValues.put(COL2, editedEntry.getAmount());
        contentValues.put(COL4, editedEntry.getDate());
        contentValues.put(COL5, String.valueOf(Calendar.getInstance().getTime()));

        db.update(TABLENAME,contentValues,COLID+" = ?",new String[]{Integer.toString(editedEntry.getId())});
    }

    public void deleteEntry(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLENAME, COLID + " = ?", new String[]{String.valueOf(id)});
    }

    public void deleteAllJarEntries(int jarId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLENAME, COL6 + " = ?", new String[]{String.valueOf(jarId)});
    }

    public List<Entry> getEntriesWithCategory(int id, int category)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME + " WHERE " + COL6
                + " = " + id + " AND " + COL3 + " = " + category;
        Cursor data = db.rawQuery(query,null);

        List<Entry> entryList = new ArrayList<>();

        while (data.moveToNext())
        {
            Entry retrievedEntry = new Entry();
            retrievedEntry.setId(data.getInt(0));
            retrievedEntry.setDescription(data.getString(1));
            retrievedEntry.setAmount(data.getDouble(2));
            retrievedEntry.setCategory(data.getInt(3));
            retrievedEntry.setDate(data.getString(4));
            retrievedEntry.setJarId(data.getInt(5));
            entryList.add(retrievedEntry);
        }

        data.close();

        return entryList;
    }

    public List<Entry> getAllEntries(int id, boolean ascending)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME + " WHERE " + COL6
                + " = " + id + " ORDER BY " + COL5;
        if (ascending)
        {
            query += " ASC";
        }
        else
        {
            query += " DESC";
        }

        Cursor data = db.rawQuery(query,null);

        List<Entry> entryList = new ArrayList<>();

        while (data.moveToNext())
        {
            Entry retrievedEntry = new Entry();
            retrievedEntry.setId(data.getInt(0));
            retrievedEntry.setDescription(data.getString(1));
            retrievedEntry.setAmount(data.getDouble(2));
            retrievedEntry.setCategory(data.getInt(3));
            retrievedEntry.setDate(data.getString(4));
            retrievedEntry.setTime(data.getString(5));
            retrievedEntry.setJarId(data.getInt(6));
            entryList.add(retrievedEntry);
        }

        data.close();

        return entryList;
    }

    public Entry getEntry(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLENAME + " WHERE " + COLID + " = " + id;
        Cursor data = db.rawQuery(query,null);

        Entry retrievedEntry = new Entry();

        if (data.moveToFirst())
        {
            do {
                retrievedEntry.setId(data.getInt(0));
                retrievedEntry.setDescription(data.getString(1));
                retrievedEntry.setAmount(data.getDouble(2));
                retrievedEntry.setCategory(data.getInt(3));
                retrievedEntry.setDate(data.getString(4));
                retrievedEntry.setTime(data.getString(5));
                retrievedEntry.setJarId(data.getInt(6));
            } while(data.moveToNext());
        }

        data.close();

        return retrievedEntry;
    }

    public double getTotalJarContent(int jarId)
    {
        List<Entry> incomeList = getEntriesWithCategory(jarId, CategoryController.income.getId());
        List<Entry> expenseList = getEntriesWithCategory(jarId, CategoryController.expense.getId());

        double total = 0;
        double deduction = 0;

        for (int counter = 0; counter < incomeList.size(); counter++)
        {
            total += incomeList.get(counter).getAmount();
        }

        for (int counter = 0; counter < expenseList.size(); counter++)
        {
            deduction += expenseList.get(counter).getAmount();
        }

        return total - deduction;
    }

    public List<String> getAllDates(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT DISTINCT "+ COL4 +" FROM "+ TABLENAME + " WHERE " + COL6
                + " = " + id + " ORDER BY " + COL4 + " DESC";
        Cursor data = db.rawQuery(query,null);

        List<String> dateList = new ArrayList<>();

        while (data.moveToNext())
        {
            dateList.add(data.getString(0));
        }

        data.close();

        return dateList;
    }
}

package com.seguewaysoft.moneyjars.controller;

import com.seguewaysoft.moneyjars.model.Category;

public class CategoryController {

    public static Category income = new Category();
    public static Category expense = new Category();

    public static void setCategories(){
        CategoryController.income.setId(1);
        CategoryController.income.setName("Income");

        CategoryController.expense.setId(2);
        CategoryController.expense.setName("Expense");
    }

}

package com.seguewaysoft.moneyjars.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.RoutineExtensions;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.controller.RoutineController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.model.Routine;

import java.util.ArrayList;
import java.util.Calendar;

public class RoutineAlertReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        int routineId = intent.getExtras().getInt("Routine_ID");
        if (routineId > 0){
            activateRoutine(context, routineId);
        }
    }

    private void activateRoutine(Context context, int routineId)
    {
        RoutineController routineController = new RoutineController(context);
        EntryController entryController = new EntryController(context);

        Routine activeRoutine = routineController.getRoutine(routineId);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        ArrayList<Entry> entries = new ArrayList<>(activeRoutine.getEntries());

        for (Entry routineEntry : entries)
        {
            routineEntry.setDate(Constants.dateFormat.format(calendar.getTime()));
            entryController.addEntry(routineEntry);
        }

        RoutineExtensions.scheduleRoutine(context, activeRoutine);
    }
}

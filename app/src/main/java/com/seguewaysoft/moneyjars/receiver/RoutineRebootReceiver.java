package com.seguewaysoft.moneyjars.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.seguewaysoft.moneyjars.RoutineExtensions;
import com.seguewaysoft.moneyjars.controller.RoutineController;
import com.seguewaysoft.moneyjars.model.Routine;

import java.util.ArrayList;

public class RoutineRebootReceiver extends BroadcastReceiver {

    RoutineController routineController;

    @Override
    public void onReceive(Context context, Intent intent) {
        routineController = new RoutineController(context);
        ArrayList<Routine> activeRoutines = routineController.getActiveRoutines();
        for (Routine routine : activeRoutines)
        {
            RoutineExtensions.scheduleRoutine(context, routine);
        }
    }
}

package com.seguewaysoft.moneyjars;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.AlarmManagerCompat;

import com.seguewaysoft.moneyjars.receiver.RoutineAlertReceiver;
import com.seguewaysoft.moneyjars.model.Routine;

import java.util.Calendar;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class RoutineExtensions {

    public static Routine scheduleRoutine(Context context, Routine routine)
    {
        Calendar currentCalendar = Calendar.getInstance();
        Calendar routineCalendar = getNextAlarmTime(context, routine);

        int routineAlarmId = (int)currentCalendar.getTimeInMillis();
        Intent routineAlertReceiverIntent = new Intent(context, RoutineAlertReceiver.class);
        routineAlertReceiverIntent.putExtra("Routine_ID", routine.getId());
        routineAlertReceiverIntent.putExtra("Alarm_ID", routineAlarmId);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, routineAlarmId, routineAlertReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        AlarmManagerCompat.setExactAndAllowWhileIdle(alarmManager, AlarmManager.RTC_WAKEUP, routineCalendar.getTimeInMillis(), pendingIntent);


        return routine;
    }

    private static Calendar getNextAlarmTime(Context context, Routine routine)
    {
        Calendar currentCalendar = Calendar.getInstance();
        Calendar routineCalendar = Extensions.convertStringToTime(context, routine.getTime());

        boolean[] alarmDays = new boolean[7];

        for (int i = 0; i < 7; i++) {
            for (MaterialDayPicker.Weekday day : routine.getDays()){
                if (day.ordinal() == i){
                    alarmDays[i] = true;
                    break;
                }
                alarmDays[i] = false;
            }
        }

        while (currentCalendar.after(routineCalendar))
            routineCalendar.add(Calendar.DATE, 1);

        int nextDay = routineCalendar.get(Calendar.DAY_OF_WEEK) - 1; // index on 0-6, rather than the 1-7 returned by Calendar

        for (int i = 0; i < 7 && !alarmDays[nextDay]; i++) {
            nextDay++;
            nextDay %= 7;
        }

        routineCalendar.set(Calendar.DAY_OF_WEEK, nextDay + 1); // + 1 = back to 1-7 range

        while (currentCalendar.after(routineCalendar))
            routineCalendar.add(Calendar.DATE, 7);

        return routineCalendar;

    }

    public static Routine removeRoutineSchedule(Context context, Routine routine)
    {
        Intent routineAlertReceiverIntent = new Intent(context, RoutineAlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, routine.getAlarmId(), routineAlertReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        return routine;
    }
}

package com.seguewaysoft.moneyjars.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.seguewaysoft.moneyjars.view.jar.JarViewer;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.EntryController;
import com.seguewaysoft.moneyjars.model.Jar;

import java.util.List;

public class JarRecyclerViewAdapter extends RecyclerView.Adapter<JarRecyclerViewAdapter.ViewHolder>
{
    private List<Jar> jars;
    private Context context;

    private EntryController entryController;

    public JarRecyclerViewAdapter(Context context, List<Jar> jars)
    {
        this.context = context;
        this.jars = jars;

        entryController = new EntryController(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.jar_recycler_view_item, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position)
    {
        holder.jarNameTextView.setText(jars.get(position).getName());

        double total = entryController.getTotalJarContent(jars.get(position).getId());
        holder.jarAmountTextView.setText(context.getString(R.string.jar_total_amount, String.format("%.2f", total)));

        holder.itemLayout.setOnClickListener(v -> {
            Intent jarViewer = new Intent(context, JarViewer.class);
            jarViewer.putExtra("jarId", jars.get(position).getId());
            context.startActivity(jarViewer);
        });
    }

    @Override
    public int getItemCount() {
        return jars.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout itemLayout;
        TextView jarNameTextView;
        TextView jarAmountTextView;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.JarItemLayout);

            jarNameTextView = itemView.findViewById(R.id.JarNameTextView);
            jarAmountTextView = itemView.findViewById(R.id.JarTotalAmount);
        }
    }
}

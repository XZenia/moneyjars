package com.seguewaysoft.moneyjars.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.seguewaysoft.moneyjars.R;

import java.util.List;

public class CardViewListAdapter extends RecyclerView.Adapter<CardViewListAdapter.ViewHolder>
{
    private List<LinearLayout> linearLayoutList;

    public CardViewListAdapter(List<LinearLayout> linearLayoutList)
    {
        this.linearLayoutList = linearLayoutList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout_recyclerview_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.itemLayout.addView(linearLayoutList.get(position));
    }

    @Override
    public int getItemCount()
    {
        return linearLayoutList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        CardView itemLayout;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.CardViewItemLayout);
        }
    }
}

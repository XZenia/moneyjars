package com.seguewaysoft.moneyjars.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.seguewaysoft.moneyjars.Constants;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.model.Entry;
import com.seguewaysoft.moneyjars.view.routine.EditRoutine;

import java.util.List;
import java.util.Locale;

public class RoutineRecyclerViewAdapter extends RecyclerView.Adapter<RoutineRecyclerViewAdapter.ViewHolder>
{
    private List<Entry> entries;
    private Context context;

    private int routineId;

    private String TAG = "EntryRecyclerViewAdapter";

    public RoutineRecyclerViewAdapter(Context context, Activity activity, List<Entry> entries, int routineId)
    {
        this.context = context;
        this.entries = entries;
        this.routineId = routineId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_recycler_view_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        holder.entryNameTextView.setText(entries.get(position).getDescription());

        if (entries.get(position).getCategory() == CategoryController.income.getId())
        {
            holder.entryAmountTextView.setText(String.format(Locale.getDefault(), "+ %.2f", entries.get(position).getAmount()));
        }
        else
        {
            holder.entryAmountTextView.setText(String.format(Locale.getDefault(), "- %.2f", entries.get(position).getAmount()));
        }

        holder.itemLayout.setOnClickListener(v -> {
            Intent editRoutine = new Intent(context, EditRoutine.class);

            editRoutine.putExtra("routineId", routineId);
            ((Activity) context).startActivityForResult(editRoutine, Constants.STATUS_OK);
        });
    }

    @Override
    public int getItemCount()
    {
        return entries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout itemLayout;
        TextView entryNameTextView;
        TextView entryAmountTextView;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.EntryItemLayout);

            entryNameTextView = itemView.findViewById(R.id.DescriptionTextView);
            entryAmountTextView = itemView.findViewById(R.id.AmountTextView);
        }
    }
}

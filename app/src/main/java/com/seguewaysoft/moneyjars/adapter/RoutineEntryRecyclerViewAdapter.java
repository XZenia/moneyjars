package com.seguewaysoft.moneyjars.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.seguewaysoft.moneyjars.Extensions;
import com.seguewaysoft.moneyjars.R;
import com.seguewaysoft.moneyjars.controller.CategoryController;
import com.seguewaysoft.moneyjars.model.Entry;

import java.util.List;
import java.util.Locale;

public class RoutineEntryRecyclerViewAdapter extends RecyclerView.Adapter<RoutineEntryRecyclerViewAdapter.ViewHolder>
{
    private List<Entry> entries;
    private Context context;

    private String TAG = "RoutineEntryRecyclerViewAdapter";

    public RoutineEntryRecyclerViewAdapter(Context context, List<Entry> entries)
    {
        this.context = context;
        this.entries = entries;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_recycler_view_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        holder.entryNameTextView.setText(entries.get(position).getDescription());

        if (entries.get(position).getCategory() == CategoryController.income.getId())
        {
            holder.entryAmountTextView.setText(String.format(Locale.getDefault(), "+ %.2f", entries.get(position).getAmount()));
        }
        else
        {
            holder.entryAmountTextView.setText(String.format(Locale.getDefault(), "- %.2f", entries.get(position).getAmount()));
        }

        holder.itemLayout.setOnClickListener(view -> {
            //create the dialog to be shown when the button gets clicked
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage("Enter description and amount");

            LayoutInflater inflater = LayoutInflater.from(context);
            View inputFieldsView = inflater.inflate(R.layout.routine_entry_inflater_fields, null);

            final EditText descriptionEditText = inputFieldsView.findViewById(R.id.DescriptionEditText);
            descriptionEditText.setText(entries.get(position).getDescription());

            final EditText amountEditText = inputFieldsView.findViewById(R.id.AmountEditText);
            amountEditText.setText(String.valueOf(entries.get(position).getAmount()));

            alertDialog.setPositiveButton(R.string.update,
                    (dialog, which) -> {
                        //setup the page
                        if (descriptionEditText.getText().toString().isEmpty())
                        {
                            Extensions.showSnackbar(view, context.getString(R.string.description_empty_error), Snackbar.LENGTH_SHORT);
                        }
                        else if (amountEditText.getText().toString().isEmpty())
                        {
                            Extensions.showSnackbar(view, context.getString(R.string.amount_empty_error), Snackbar.LENGTH_SHORT);
                        }
                        else
                        {
                            entries.get(position).setDescription(descriptionEditText.getText().toString());
                            entries.get(position).setAmount(Double.parseDouble(amountEditText.getText().toString()));
                            notifyDataSetChanged();
                            Extensions.showSnackbar(view, context.getString(R.string.entry_update_success_message), Snackbar.LENGTH_SHORT);
                        }
                    });

            alertDialog.setNegativeButton(R.string.delete,
                    (dialog, which) -> {
                        entries.remove(entries.get(position));
                        notifyDataSetChanged();
                    }
            );

            alertDialog.setView(inputFieldsView);
            alertDialog.show();
        });
    }

    @Override
    public int getItemCount()
    {
        return entries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout itemLayout;
        TextView entryNameTextView;
        TextView entryAmountTextView;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemLayout = itemView.findViewById(R.id.EntryItemLayout);

            entryNameTextView = itemView.findViewById(R.id.DescriptionTextView);
            entryAmountTextView = itemView.findViewById(R.id.AmountTextView);
        }
    }
}

package com.seguewaysoft.moneyjars;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM / dd / yyyy - EEEE", Locale.ENGLISH);
    public static int STATUS_OK = 1;
}
